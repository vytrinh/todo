import Vue from 'vue'
import Router from 'vue-router'
import compheader from '@/components/compheader'
import comphome from '@/components/comphome'
import testapi from '@/components/testapi'
import apibacktest from '@/components/apibacktest'
import coinapi from '@/components/coinapi'
import errorcomp from '@/components/layouts/errorcomp'
import excomp from '@/components/excomp'
import dash from '@/components/dash'
import VProvide from '@/components/VProvide'
import log from '@/components/log'
import regi from '@/components/regi'
import lout from '@/components/lout'
Vue.use(Router)
export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/header',
      name: 'compheader',
      component: compheader
    },
    {
      path: '/home',
      name: 'comphome',
      component: comphome
    },
    {
      path: '/test',
      name: 'testapi',
      component: testapi
    },
    {
      path: '/back',
      name: 'apibacktest',
      component: apibacktest
    },
    {
      path: '/coin',
      name: 'coinapi',
      component: coinapi
      // children: [{
      //   path: '/error',
      //   name: 'errorcomp',
      //   component: errorcomp
      // }]
    },
    {
      path: '/error',
      name: 'errorcomp',
      component: errorcomp
    },
    {
      path: '/exs',
      name: 'excomp',
      component: excomp
    },
    {
      path: '/dash',
      name: 'dash',
      component: dash,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/VP',
      name: 'VProvide',
      component: VProvide
    },
    {
      path: '/log',
      name: 'log',
      component: log,
      meta: {
        requiresVisitor: true
      }
    },
    {
      path: '/regi',
      name: 'regi',
      component: regi,
      meta: {
        requiresVisitor: true
      }
    },
    {
      path: '/lout',
      name: 'lout',
      component: lout
    }
  ]
})
