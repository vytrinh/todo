/* eslint-disable */
export default {
    addEmployee({ commit }, employee) {
        commit('add_Employee', employee)
    },
    deleteEmployee({ commit }, id) {
        commit('delete_employee', id)
    },
    updateEmployee({ commit }, employee) {
        commit('update_employee', employee)
    }
    // setL ({commit}, payload) {
    //   commit('SET_L', payload)
    // }
}
// export const addEmployee = ({commit}, employee) => {
//     commit('add_Employee', employee)
// }