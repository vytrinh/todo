/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
// import app from './../main'
import axios from 'axios'
axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/'
Vue.use(Vuex)
const loadState = () => {
  try {
    var empState = localStorage.getItem('employee')
    if (empState === null || empState === '[]') {
      return undefined
    }
    return JSON.parse(empState)
  } catch (err) {
    return undefined
  }
}

const saveState = (state) => {
  try {
    const empState = JSON.stringify(state)
    localStorage.setItem('employee', empState)
  } catch (err) {
    console.error(`Something went wrong: ${err}`)
  }
}
export const store = new Vuex.Store({
  state: {
    employeeList: loadState() || [{
      id: 1,
      name: 'Vỹ Trịnh',
      phone: '0987257590',
      address: 'Gò Vấp'
    }],
    token: localStorage.getItem('access_token') || null
  },
  getters: {
    allemployee: state => {
      return state.employeeList
    },
    loggedIn (state) {
      return state.token !== null
    }
  },
  actions: {
    addEmployee ({commit}, employee) {
      commit('add_Employee', employee)
    },
    deleteEmployee ({commit}, id) {
      commit('delete_employee', id)
    },
    updateEmployee ({commit}, employee) {
      commit('update_employee', employee)
    },
    destroyToken (context) {
      axios.defaults.headers.common['Authorization'] = 'Bearer' + context.state.token
      if(context.getters.loggedIn) {
        return new Promise((resolve, reject) => {
        axios.post('photos')
        .then(response => {
          localStorage.removeItem('access_token')
          context.commit('destroyToken')
          resolve(response)
          console.log(response)
        })
        .catch (error => {
          localStorage.removeItem('access_token')
          context.commit('destroyToken')
          reject(error)
        })
      })
      }
    },
    retrieveToken (context, credentials) {
      return new Promise((resolve, reject) => {
      axios.post('users', {
        username: credentials.username,
        password: credentials.password
      })
      .then(response => {
        const token = response.data.access_token
        localStorage.setItem('access_token', token)
        context.commit('retrieveToken', token)
        resolve(response)
        console.log(response)
      })
      .catch (error => {
        console.log(error)
        reject(error)
      })
    })
    },
    register (context, data) {
      return new Promise((resolve, reject) => {
        axios.post('comments', {
          username: data.username,
          password: data.password,
          email: data.email
        })
        .then(response => {
          resolve(response)
          console.log(response)
        })
        .catch (error => {
          reject(error)
        })
      })
    }
    // setL ({commit}, payload) {
    //   commit('SET_L', payload)
    // }
  },
  mutations: {
    add_Employee (state, employee) {
      state.employeeList.push(employee)
      // console.log(employee)
    },
    delete_employee (state, id) {
      let index = state.employeeList.map(demp => demp.id).indexOf(id)
      state.employeeList.splice(index, 1)
    },
    update_employee (state, employee) {
      let index = state.employeeList.findIndex((item) => item.id === employee.id)
      if (index >= 0) {
        state.employeeList.splice(index, 1, employee)
      }
      console.log(index)
    },
    retrieveToken (state, token) {
      state.token = token
    },
    destroyToken (state) {
      state.token = null
    }
    // SET_L (state, payload) {
    //   state.app.$i18n.locale = payload
    // }
  }
})
store.watch(
  (state) => state.employeeList,
  (state) => {
    saveState(state)
  }
)
