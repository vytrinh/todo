/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
// import app from './../main'
// import axios from 'axios'
// axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/'
// import getters from './getters'
// import actions from './actions'
// import mutations from './mutations'
Vue.use(Vuex)
const loadState = () => {
  try {
    var empState = localStorage.getItem('employee')
    if (empState === null || empState === '[]') {
      return undefined
    }
    return JSON.parse(empState)
  } catch (err) {
    return undefined
  }
}

const saveState = (state) => {
  try {
    const empState = JSON.stringify(state)
    localStorage.setItem('employee', empState)
  } catch (err) {
    console.error(`Something went wrong: ${err}`)
  }
}
export const store = new Vuex.Store({
  state: {
    employeeList: loadState() || [{
      id: 1,
      name: 'Vỹ Trịnh',
      phone: '0987257590',
      address: 'Gò Vấp'
    }]
  },
  getters: {
    allemployee: state => {
      return state.employeeList
    }
  },
  actions: {
    addEmployee ({commit}, employee) {
      commit('add_Employee', employee)
    },
    deleteEmployee ({commit}, id) {
      commit('delete_employee', id)
    },
    updateEmployee ({commit}, employee) {
      commit('update_employee', employee)
    }
    // setL ({commit}, payload) {
    //   commit('SET_L', payload)
    // }
  },
  mutations: {
    add_Employee (state, employee) {
      state.employeeList.push(employee)
      // console.log(employee)
    },
    delete_employee (state, id) {
      let index = state.employeeList.map(demp => demp.id).indexOf(id)
      state.employeeList.splice(index, 1)
    },
    update_employee (state, employee) {
      let index = state.employeeList.findIndex((item) => item.id === employee.id)
      if (index >= 0) {
        state.employeeList.splice(index, 1, employee)
      }
      console.log(index)
    }
    // SET_L (state, payload) {
    //   state.app.$i18n.locale = payload
    // }
  }
})
store.watch(
  (state) => state.employeeList,
  (state) => {
    saveState(state)
  }
)
