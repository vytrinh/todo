/* eslint-disable */
export default {
    add_Employee(state, employee) {
        state.employeeList.push(employee)
        // console.log(employee)
    },
    delete_employee(state, id) {
        let index = state.employeeList.map(demp => demp.id).indexOf(id)
        state.employeeList.splice(index, 1)
    },
    update_employee(state, employee) {
        let index = state.employeeList.findIndex((item) => item.id === employee.id)
        if (index >= 0) {
            state.employeeList.splice(index, 1, employee)
        }
        console.log(index)
    }
    // SET_L (state, payload) {
    //   state.app.$i18n.locale = payload
    // }
}
// export const add_Employee = (state, employee) => {
//     state.employeeList.push(employee)
// }